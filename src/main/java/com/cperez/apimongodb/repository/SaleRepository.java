package com.cperez.apimongodb.repository;


import com.cperez.apimongodb.model.Sale;

import java.util.List;

public interface SaleRepository {
	public List<Sale> getSales();
	public Sale createSale(Sale sale);
}
