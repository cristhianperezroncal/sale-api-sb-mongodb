package com.cperez.apimongodb.repository.impl;

import com.cperez.apimongodb.model.Sale;
import com.cperez.apimongodb.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SaleRepositoryImpl implements SaleRepository {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public List<Sale> getSales() {
        return mongoTemplate.findAll(Sale.class);
    }

    @Override
    public Sale createSale(Sale sale) {
        return mongoTemplate.insert(sale);
    }
}
