package com.cperez.apimongodb.service;


import com.cperez.apimongodb.dto.SaleDTO;
import com.cperez.apimongodb.model.Sale;

import java.util.List;

public interface SaleService {
	public List<Sale> getSales();
	public Sale createSale(SaleDTO saleDTO);
}
